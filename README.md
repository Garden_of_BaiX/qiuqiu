# `大球吃小球`项目

## 项目简介
模仿球球大作战经典玩法的C语言(easyx图形库)小游戏
## 目录结构说明
documents(项目相关文档，如：需求文档，功能流程图等）

## 环境搭配
### 开发工具
|工具|说明|版本|备注|
|----|----|----|----|
|VS 2022|开发IDE|???||
|墨刀|原型设计工具|???||
|ProcessOn|流程图绘制工具|???||

## 开发进度


